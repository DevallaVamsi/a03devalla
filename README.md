# a03Devalla running personal website on a local server


## How to use

Open a command window in your a03Devalla folder.

Run npm install to install all the dependencies in the package.json file.

Run node server.js to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> node server.js
```

Point your browser to `http://localhost:8081`.